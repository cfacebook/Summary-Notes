#  在Jenkins的Pipeline中，sh函数的用法

##  用法一

单个命令字符串包括使用，示例如下：

```groovy
sh 'echo "Hello, Jenkins!"'
```

### 用法二

多个命令字符串包括命令列表使用，示例如下：

```groovy
sh '''
echo "Step 1"
echo "Step 2"
echo "Step 3"
'''
```

这种用法允许你在多行中写入Shell命令。

## 用法三

类似于函数调用的使用方法，示例如下：

```groovy
def myVariable = sh(script: 'echo "Hello, Jenkins!"', returnStdout: true).trim()
```

这种用法将Shell脚本的输出捕获到Jenkins Pipeline中的一个变量中。

入参及其含义：

- `script`: 这是要执行的Shell脚本命令。它可以是单个命令字符串，也可以是包含多个命令的多行字符串。例如：`script: 'echo "Hello, Jenkins!"'` 或 `script: 'echo "Step 1"\necho "Step 2"\necho "Step 3"'`。

- `returnStatus` (可选): 如果设置为true，Jenkins将返回Shell脚本的退出状态而不是默认的标准输出。通常，如果脚本成功执行，它将返回0。默认值为false。

- `returnStdout` (可选): 如果设置为true，Jenkins将捕获Shell脚本的标准输出并将其返回。这样可以将脚本输出存储在一个变量中。默认值为false。

- `returnStderr` (可选): 如果设置为true，Jenkins将捕获Shell脚本的标准错误输出并将其返回。默认值为false。

- `outpustFile` (可选): 允许将Shell脚本的输出写入指定的文件。例如：`outputFile: 'output.txt'`。

- `label`: 可以用来指定在Jenkins构建日志中显示的自定义标签。例如：

  ```groovy
  sh label: 'My Custom Label', script: 'echo "Hello, Jenkins!"'
  ```


注意：当使用多行字符串（triple-single-quoted或triple-double-quoted）时，Jenkins会按原样执行其中的内容，包括空格和缩进，因此请小心使用。



其他用法，参考：

[jenkins pipeline中获取shell命令的标准输出或者状态]: https://blog.csdn.net/QQ70945934/article/details/131925735	"jenkins pipeline中获取shell命令的标准输出或者状态"

