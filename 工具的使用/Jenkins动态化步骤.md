#  Jenkins中如何去根据入参动态化步骤呢？

##  Groovy语言基础

1. 定义一个列表变量

   ```groovy
   def list = []
   ```

2. 定义一个`map`的`kv`结构变量

   ```groovy
   def map = [:]
   ```

## 如何可以动态化步骤（steps）

动态化步骤：其实就是，在`jenkins pipeline`中根据入参或者其他变量列表，动态化的生成步骤列表。比如：在一个job中拉取多个仓库的代码，仓库的数量是不确定的，根据环境变量参数动态任意自定义的。

## 利用parallel函数实现

parallel函数声明式的写法如下：

```groovy
pipeline {
    agent none
    stages {
        stage('Run Tests') {
            parallel {
                stage('Test On Windows') {
                    agent {
                        label "windows"
                    }
                    steps {
                        bat "run-tests.bat"
                    }
                    post {
                        always {
                            junit "**/TEST-*.xml"
                        }
                    }
                }
                stage('Test On Linux') {
                    agent {
                        label "linux"
                    }
                    steps {
                        sh "run-tests.sh"
                    }
                    post {
                        always {
                            junit "**/TEST-*.xml"
                        }
                    }
                }
            }
        }
    }
}
```

那如何用代码拼装任务列表呢？其实声明式写法，也是调用一个函数将参数传递。所以就可以定义一个闭包函数列表的方式去调用`parallel `函数

代码写法如下：

```groovy
pipeline {
    agent any
    stages {
        stage("dynamic-steps") {
            steps {
                script {
                    def taskList = [:]
                    def list = ["task1", "task2"].toList()
                    for (i = 0; i < list.size(); i++) {
                        echo "${i}"
                        def index = i;
                        def taskName = list[i]
                        taskList["${taskName}"] = {
                            echo("${index}=========${taskName}")
                        }
                    }
                    echo "${toJson(taskList)}"
                    parallel(taskList)
                }
            }
        }
    }

}
```

其中入参是一个类似`map`的`kv`结构。

1. key:  就是任务的名称
2. value: 闭包函数，就是要执行的任务函数

在Blue Ocean中的效果大概如下（不是上面代码生成效果图，是我实际应用中产生的效果图）：
![](./assets/1689588987486.png)

