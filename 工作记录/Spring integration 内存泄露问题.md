#  Spring-integration-redis 框架内存泄露问题记录

## 背景及现象

服务端应用只要重启时间超过两周就会发生内存使用率100%，并且出现长时间的full-gc。通过zabbix观察内存情况如下图：

![1670846134647](E:\myself\Summary-Notes\工作记录\assets\1670846134647.png)

## 排查过程

###  制作堆Dump文件

内存有问题，所以第一时间肯定是叫运维保留现场（即dump内存信息），然后分析dump出来的.hprof文件。如果没有运维，那就只能自己去服务器dump内存信息了。

dump内存的方法：

1. 执行`jps`命令得到`java`的进程ID，当然也可以用其他命令得到应用的进程ID（例如：`ps`命令）；

2. 执行`jmap`命令制作堆Dump

   ```shell
   jmap -dump:format=b,file=文件名.hprof <步骤1得到的进程ID>
   ```

### 分析内存情况

本次介绍分析内存的工具，为三款分别是：`jvisualvm`(`java`自带工具)、mat和IDEA的功能。

#### jvisualvm工具

本工具是`java`自带的工具，在`{java-home}/bin`目录下，如下图：

![1670847255098](E:\myself\Summary-Notes\工作记录\assets\1670847255098.png)

通过上图可以看出，该工具只能windows系统下使用，双击图标启动软件，然后加载dump文件进行分析

![1670847585419](E:\myself\Summary-Notes\工作记录\assets\1670847585419.png)

![1670847695614](E:\myself\Summary-Notes\工作记录\assets\1670847695614.png)

加载过程可能会内存不足，内存不足就会弹框：

![1670850149817](E:\myself\Summary-Notes\工作记录\assets\1670850149817.png)

**内存不足的解决方法：**

```shell
修改JAVA_HOME/lib/visualvm/etc/visualvm.conf文件中 visualvm_default_options="-J-client -J-Xms24 -J-Xmx256m",把256改为2048，然后重启jvisualVM即可。
```

具体如何使用，自行百度资料更详细，本文并不详细介绍。

#### MemoryAnalyzer(MAT)工具

```shell
location /api/ltpoint {

}
```



[参考] [jvisualvm 提示 堆查看器使用的内存不足](https://blog.csdn.net/bobocqu/article/details/124508949)

[参考] 